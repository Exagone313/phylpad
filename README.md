# Phylpad

Phylpad is a simple plain text editor, inspired by [Leafpad](http://tarot.freeshell.org/leafpad/).

## Requirements

* Zig 0.11
* gtk4
* glib-2.0 (comes with gtk4)
* gtksourceview-5

## License

Copyright (c) 2022 Elouan Martinet <exa@elou.world>

Phylpad is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Phylpad is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Phylpad. If not, see <https://www.gnu.org/licenses/>.
