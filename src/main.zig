// Copyright (c) 2022 Elouan Martinet <exa@elou.world>
// SPDX-License-Identifier: GPL-3.0+

const std = @import("std");
const print = std.debug.print;
const gtk = @cImport({
    @cInclude("gtk/gtk.h");
    @cInclude("glib.h");
    @cInclude("gtksourceview/gtksource.h");
});

const Config = struct {
    window_width: gtk.gint = 640,
    window_height: gtk.gint = 400,
    editor_show_line_numbers: bool = true,
    editor_indent_on_tab: bool = true,
    editor_wrap_mode: gtk.gint = gtk.GTK_WRAP_WORD_CHAR,
    font_family: [*:0]const u8 = "monospace",
    font_size: [*:0]const u8 = "100%",
};

pub fn main() !void {
    var config = Config {};
    read_config(&config);

    const app = gtk.gtk_application_new(
        "app.ewd.phylpad",
        gtk.G_APPLICATION_NON_UNIQUE,
    );
    if (app == null) {
        @panic("Failed to create GTK application");
    }
    defer gtk.g_object_unref(app);

    _ = gtk.g_signal_connect_data(
        app,
        "activate",
        @ptrCast(&activate),
        &config,
        null,
        0,
    );

    const status = gtk.g_application_run(
        @ptrCast(app),
        0,
        null,
    );
    std.process.exit(@intFromBool(status != 0));
}

fn read_config(config: *Config) void {
    const key_file = gtk.g_key_file_new();
    if (key_file == null) {
        @panic("Failed to create GKeyFile");
    }
    defer gtk.g_key_file_free(key_file);
    const user_config_dir = gtk.g_get_user_config_dir();
    const file_path = std.fmt.allocPrintZ(
        std.heap.page_allocator,
        "{s}/phylpad/config",
        .{user_config_dir},
    ) catch @panic("Failed to allocate config path");
    var has_errors: ?*gtk.GError = null;
    const return_value = gtk.g_key_file_load_from_file(
        @ptrCast(key_file),
        file_path.ptr,
        gtk.G_KEY_FILE_KEEP_COMMENTS | gtk.G_KEY_FILE_KEEP_TRANSLATIONS,
        &has_errors,
    );
    if (return_value == 0) {
        if (has_errors) |error_value| {
            print(
                "Could not open config file at {s}: {s}\n",
                .{file_path, error_value.message},
            );
        }
        return;
    }

    // window_width
    has_errors = null;
    const window_width = gtk.g_key_file_get_integer(
        key_file,
        "window",
        "width",
        &has_errors,
    );
    if (has_errors == null) {
        config.window_width = window_width;
    }
    // window_height
    has_errors = null;
    const window_height = gtk.g_key_file_get_integer(
        key_file,
        "window",
        "height",
        &has_errors,
    );
    if (has_errors == null) {
        config.window_height = window_height;
    }
    // editor_show_line_numbers
    has_errors = null;
    const editor_show_line_numbers = gtk.g_key_file_get_boolean(
        key_file,
        "editor",
        "show_line_numbers",
        &has_errors,
    );
    if (has_errors == null) {
        config.editor_show_line_numbers = editor_show_line_numbers != 0;
    }
    // editor_indent_on_tab
    has_errors = null;
    const editor_indent_on_tab = gtk.g_key_file_get_boolean(
        key_file,
        "editor",
        "indent_on_tab",
        &has_errors,
    );
    if (has_errors == null) {
        config.editor_indent_on_tab = editor_indent_on_tab != 0;
    }
    // editor_wrap_mode
    has_errors = null;
    const editor_wrap_mode = gtk.g_key_file_get_integer(
        key_file,
        "editor",
        "wrap_mode",
        &has_errors,
    );
    if (has_errors == null) {
        config.editor_wrap_mode = editor_wrap_mode;
    }
    // font_family
    has_errors = null;
    const font_family = gtk.g_key_file_get_string(
        key_file,
        "font",
        "family",
        &has_errors,
    );
    if (has_errors == null) {
        config.font_family = font_family;
    }
    // font_size
    has_errors = null;
    const font_size = gtk.g_key_file_get_string(
        key_file,
        "font",
        "size",
        &has_errors,
    );
    if (has_errors == null) {
        config.font_size = font_size;
    }
}

fn activate(app: *gtk.GtkApplication, config: *Config) callconv(.C) void {
    const window = gtk.gtk_application_window_new(app);
    gtk.gtk_window_set_title(@ptrCast(window), "Phylpad");
    gtk.gtk_window_set_default_size(
        @ptrCast(window),
        config.window_width,
        config.window_height,
    );

    const buffer = gtk.gtk_source_buffer_new(null);
    gtk.gtk_source_buffer_set_style_scheme(buffer, null);

    const view = gtk.gtk_source_view_new_with_buffer(buffer);
    gtk.gtk_source_view_set_show_line_numbers(
        @ptrCast(view),
        @intFromBool(config.editor_show_line_numbers),
    );
    gtk.gtk_source_view_set_indent_on_tab(
        @ptrCast(view),
        @intFromBool(config.editor_indent_on_tab),
    );
    gtk.gtk_text_view_set_wrap_mode(
        @ptrCast(view),
        @intCast(config.editor_wrap_mode),
    );

    const css_provider = gtk.gtk_css_provider_new();
    defer gtk.g_object_unref(css_provider);
    const css = std.fmt.allocPrintZ(
        std.heap.page_allocator,
        \\textview {{
        \\  font-family: {s};
        \\  font-size: {s};
        \\}}
        ,
        .{
            config.font_family,
            config.font_size,
        },
    ) catch @panic("Failed to allocate CSS string");
    gtk.gtk_css_provider_load_from_data(
        @ptrCast(css_provider),
        css.ptr,
        -1,
    );
    gtk.gtk_style_context_add_provider(
        gtk.gtk_widget_get_style_context(@ptrCast(view)),
        @ptrCast(css_provider),
        gtk.GTK_STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    const sw = gtk.gtk_scrolled_window_new();
    gtk.gtk_scrolled_window_set_policy(
        @ptrCast(sw),
        gtk.GTK_POLICY_AUTOMATIC,
        gtk.GTK_POLICY_AUTOMATIC,
    );
    gtk.gtk_scrolled_window_set_overlay_scrolling(
        @ptrCast(sw),
        0,
    );

    gtk.gtk_window_set_child(@ptrCast(window), sw);
    gtk.gtk_scrolled_window_set_child(
        @ptrCast(sw),
        view,
    );

    gtk.gtk_window_present(@ptrCast(window));
}
